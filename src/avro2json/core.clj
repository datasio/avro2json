(ns avro2json.core
  (:require [abracad.avro :as avro]
            [cheshire.core :as json])
  (:gen-class))


(defn read-avro [fpath]
  (with-open [adf (avro/data-file-reader fpath)]
     (doall (seq adf))))

(defn -main
  [& avro-files]
  (doseq [f avro-files]
    (doseq [datum (read-avro f)]
      (println (json/generate-string datum)))))

