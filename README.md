# avro2json

Convert avro to json

## Download

https://bitbucket.org/datasio/avro2json/downloads/avro2json.jar

## Usage

    $ java -jar avro2json.jar /home/user/Downloads/events.1455891172189.avro > output.json

