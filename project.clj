(defproject avro2json "1.0"
  :description "Convert avro file to json"
  :url "https://bitbucket.org/datasio/avro2json"
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [com.damballa/abracad "0.4.13"]
                 [cheshire "5.5.0"]]
  :main ^:skip-aot avro2json.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
